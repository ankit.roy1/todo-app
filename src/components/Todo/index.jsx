import React from 'react'
import { useState } from 'react';
import uuid from 'react-uuid';
import styles from "./Todo.module.css"

const Todo = ({ todo,subTasks,getSubTasks, onDeleteTodo, onCompleteTodo,addChildren }) => {
  const [isOpen,setIsOpen]=useState(false)
  const [subTopic,setSubTopic]=useState("")

  const handleSubtopic = (id) => {
    if (subTopic.trim() === "") {
      alert("Please enter a valid todo")
      return
    }
    
    const newSubTodo = {
      id: uuid(),
      parentId:id,
      task: subTopic,
      isCompleted:false
    }

    addChildren(id, newSubTodo)
    setSubTopic("")
    setIsOpen((prev) => !prev)
  }
  return (
    <>
      <li
        className={`${styles.task} ${todo.isCompleted ? styles.completed : ''}`}
        style={{width:'100%'}}
      >
      <input
        type="checkbox"
        checked={todo.isCompleted}
        className={styles.task__checkbox}
        onChange={(e) => onCompleteTodo(todo.id)}
      />
      <span className={styles.task__name}>
        {todo.task}
      </span>
      <button 
          type="button"
          onClick={(e) => setIsOpen((prev)=>!prev)}
          className={ styles.task__button} 
        >Add SubTopic</button>
      <button
        type="button"
        className={styles.task__button}
        onClick={() => onDeleteTodo(todo.id)}
      >
        Delete
      </button>
      </li>
      {subTasks.length > 0 && (
        <li
          className={`${styles.task} ${styles.subtask} ${todo.isCompleted ? styles.completed : ''}`}
          style={{width:'100%'}}
        >
          <ul className={styles.tasks}>
            {subTasks.map(todo => (
              <Todo
                key={todo.id}
                todo={todo}
                getSubTasks={getSubTasks}
                subTasks={getSubTasks(todo.id)}
                addChildren={addChildren}
                onDeleteTodo={onDeleteTodo}
                onCompleteTodo={onCompleteTodo}
              />
            ))}
          </ul>  
      </li>)}
      {isOpen && (
        <div className={styles.addSubtask}>
          <input
            type="text"
            className={styles.input}
            value={subTopic}
            style={{borderBottom:'1px solid white'}}
            onChange={(e) => setSubTopic(e.target.value)}
          />
          <button
            className={styles.task__button}
            type="button"
            onClick={(e) => handleSubtopic(todo.id)}>Add</button>  
        </div>)
      }
    </>
  )
}

export default Todo