import React, { useState } from 'react'
import uuid from "react-uuid"
import styles from "./AddTodo.module.css"

const AddTodo = ({addTodo}) => {
  const [todo, setTodo] = useState("")

  const handleSubmit = (e) => {
    e.preventDefault()
    if (todo.trim() === "") {
      alert("Please enter a valid todo")
      return;
    }

    const newTodo = {
      id: uuid(),
      task: todo,
      parentId: null,
      isCompleted:false
    }

    addTodo(newTodo)

    setTodo("")
  }

  return (
    <form className={styles.form} onSubmit={handleSubmit}>
      <input
        className={styles.input}
        type="text"
        onChange={(e) => setTodo(e.target.value)}
        value={todo}
      />
      <button className={styles.form__button} type='submit'>Add Todo</button>
    </form>      
  )
}

export default AddTodo