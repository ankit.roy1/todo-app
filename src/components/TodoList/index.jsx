import React from 'react'
import Todo from "./../Todo"
import styles from "./TodoList.module.css"

const TodoList = ({todos,onDeleteTodo,onCompleteTodo,addChildren}) => {
  //get all the subtask of a parent todo
  const getSubTasks = (parentId) => {
    const subTasks = todos.filter((todo) => todo.parentId === parentId)
    return subTasks
  }

  const rootTodos = todos.filter((todo)=>todo.parentId===null)

  return (
    <ul className={styles.tasks}>
      {rootTodos.map(todo => (
        <Todo
          key={todo.id}
          todo={todo}
          getSubTasks={getSubTasks}
          subTasks={getSubTasks(todo.id)}
          addChildren={addChildren}
          onDeleteTodo={onDeleteTodo}
          onCompleteTodo={onCompleteTodo}
        />
      ))}
    </ul>
  )
}

export default TodoList