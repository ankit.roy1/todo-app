import React, { useEffect, useState } from "react"
import styles from "./App.module.css";
import AddTodo from "./components/AddTodo";
import TodoList from "./components/TodoList";

function App() {
  const [todos, setTodos] = useState([])
  
  //Add root Todo task
  const addTodo = (todo) => {
    setTodos((prev)=>[todo,...prev])
  }

  //Delete a Todo
  const removeTodo = (id) => {
    const updatedTodos = todos.filter((todo) => todo.id !== id)
    setTodos(updatedTodos)
  }

  //Mark a todo as complete
  const completeTodo = (id) => {
    const updatedTodos = todos.map((todo) => {
      if (todo.id === id) {
        return {
          ...todo,
          isCompleted:!todo.isCompleted
        }
      }
      return todo;
    })
    setTodos(updatedTodos)
  }
  
  //Handle adding subTask to a parent task
  const addChildren = (parentId,childTask) => {
    const result = []
    for (let i = 0; i < todos.length; i++){
      let currTodo = todos[i]
      result.push(currTodo)
      if (currTodo.id === parentId) {
        result.push(childTask)
      }
    }

    setTodos(result)
  }
  return (
    <div className={styles.container}>
      <div className={styles.content}>
        <h1>Todo List</h1>
        <AddTodo addTodo={addTodo} />
        <TodoList
          addChildren={addChildren}
          todos={todos}
          onDeleteTodo={removeTodo}
          onCompleteTodo={completeTodo}
        />
      </div>
    </div>
  );
}

export default App;
